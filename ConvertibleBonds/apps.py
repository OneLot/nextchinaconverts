from django.apps import AppConfig


class ConvertibleBondsConfig(AppConfig):
    name = 'ConvertibleBonds'
