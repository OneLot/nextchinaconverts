import time
from datetime import datetime, date
from typing import Optional

import bs4
from django.core.management.base import BaseCommand
from selenium.common.exceptions import TimeoutException
from selenium.webdriver import *
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from ConvertibleBonds.serializers import ConvertibleBondRightsIssueAnnouncementSerializer


class Command(BaseCommand):
    help = 'Persists new cb announcements for trader and prime broker election'

    def add_arguments(self, parser):
        parser.add_argument('args')

    @staticmethod
    def parse_ca_announcement_date(soup: str) -> date:
        parsed_announcement_date: Optional[date] = None
        formats = ["%Y-%m-%d", "%Y-%m-%d %I:%M"]
        for soup_str_format in formats:
            try:
                parsed_announcement_date: date = datetime.strptime(soup, soup_str_format).date()
                return parsed_announcement_date
            except ValueError:
                continue
        return parsed_announcement_date

    def handle(self, *args, **options):
        url = "https://www.jisilu.cn/data/cbnew/announcement/"
        browser = Chrome()
        browser.get(url)
        delay = 5

        try:
            myElem = WebDriverWait(browser, 5).until(EC.presence_of_element_located((By.ID, 'undefined')))
            print("Page is ready!")
            browser.find_element_by_id(id_='announcement_tp_y').click()  # remove listed CBS
            browser.find_element_by_id(id_='announcement_tp_r').click()  # add pending CBS CAs
            browser.find_elements_by_xpath("//*[@type='submit' and @class='btn btn-primary btn-xs']")[0].click()
            time.sleep(2)
            html = browser.page_source
            parsed_content = bs4.BeautifulSoup(html, 'lxml')
            print('pending listings downloaded')
        except TimeoutException:
            print("Loading took too much time!")
            exit()
        finally:
            print('lambda exec done')
            browser.quit()

        # noinspection PyUnboundLocalVariable
        table = parsed_content.find('table', id="flex_announcement")
        pending_convertibles = table.findAll('tr')[2:]
        persisted_debt_codes = []

        for pending_convertible in pending_convertibles:
            obj = {
                "stock_name": pending_convertible.contents[3].text,
                "stock_code": int(pending_convertible.contents[2].text),
                "convertible_debt_code": int(pending_convertible.contents[4].text),
                "rights_issue_url": "https://www.jisilu.cn{0}".format(pending_convertible.contents[4].find('a').attrs['href']),
                "announcement_time": self.parse_ca_announcement_date(soup=pending_convertible.contents[1].text),
                "db_insertion_date": datetime.now().date(),
                "name_of_convert": pending_convertible.contents[5].text,
                "announcement": pending_convertible.contents[6].text
            }
            to_persist = ConvertibleBondRightsIssueAnnouncementSerializer(data=obj)
            if to_persist.is_valid():
                cb_code = to_persist.data["convertible_debt_code"]
                if not cb_code in persisted_debt_codes:
                    persisted_debt_codes.append(cb_code)
                    to_persist.create(validated_data=to_persist.data)
        print('command run success')
