from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())


def pkgen():
    from base64 import b32encode
    from hashlib import sha1
    from random import random
    rude = ('lol',)
    bad_pk = True
    while bad_pk:
        pk = b32encode(sha1(str(random())).digest()).lower()[:6]
        bad_pk = False
        for rw in rude:
            if pk.find(rw) >= 0: bad_pk = True
    return pk


class ConvertibleBondRightsIssueAnnouncement(models.Model):
    stock_name = models.CharField(blank=False, max_length=50)
    stock_code = models.IntegerField(blank=False)
    convertible_debt_code = models.IntegerField(blank=False, primary_key=True, default=pkgen)
    rights_issue_url = models.CharField(blank=False, max_length=200)
    announcement_time = models.DateField(blank=False, auto_now=False)
    db_insertion_date = models.DateField(blank=False, auto_now=True)
    name_of_convert = models.CharField(blank=False, max_length=100)
    announcement = models.CharField(blank=False, max_length=400)

    class Meta:
        ordering = ('-announcement_time',)
