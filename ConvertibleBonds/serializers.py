from rest_framework import serializers
from ConvertibleBonds.models import ConvertibleBondRightsIssueAnnouncement
from django.contrib.auth.models import User


class ConvertibleBondRightsIssueAnnouncementSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConvertibleBondRightsIssueAnnouncement
        fields = (
            'stock_name',
            'stock_code',
            'convertible_debt_code',
            'rights_issue_url',
            'announcement_time',
            'db_insertion_date',
            'name_of_convert',
            'announcement',
        )


class UserSerializer(serializers.HyperlinkedModelSerializer):
    snippets = serializers.HyperlinkedRelatedField(many=True,
                                                   view_name='snippet-detail',
                                                   read_only=True)

    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'index')
