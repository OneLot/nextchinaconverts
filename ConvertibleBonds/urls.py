from django.urls import path, include
from rest_framework.routers import DefaultRouter
from ConvertibleBonds import views

router = DefaultRouter()
router.register(r'convertible-bonds-rights-issue-announced', views.ConvertibleBondRightsIssueAnnouncementViewSet)
# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]
