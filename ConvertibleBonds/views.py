from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework_swagger.views import get_swagger_view

from ConvertibleBonds.models import ConvertibleBondRightsIssueAnnouncement
from ConvertibleBonds.serializers import ConvertibleBondRightsIssueAnnouncementSerializer, UserSerializer

schema_view = get_swagger_view(title='ConvertibleBonds API')


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides 'list' and 'detail' actions
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ConvertibleBondRightsIssueAnnouncementViewSet(viewsets.ViewSet):
    queryset = ConvertibleBondRightsIssueAnnouncement.objects.all()

    def list(self, request):
        convertible_bonds_rights_issues_announcements = ConvertibleBondRightsIssueAnnouncement.objects.all()
        serializer = ConvertibleBondRightsIssueAnnouncementSerializer(convertible_bonds_rights_issues_announcements, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        convertible_bonds_rights_issues_announcements = ConvertibleBondRightsIssueAnnouncement.objects.all()
        compo = get_object_or_404(convertible_bonds_rights_issues_announcements, pk=pk)
        serializer = ConvertibleBondRightsIssueAnnouncementSerializer(compo, context={'request': request})
        return Response(serializer.data)
