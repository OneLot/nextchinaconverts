# NextChinaConverts

The goal of this project is to showcase Django rest framework and expose pending corporate actions on convertible bond rights issues on China Bonds Connect that trader need to pass election to prime brokers to avoid getting arbed

## Install and Boot
```cmd
conda create -n chinabonds python=3.8
activate chinabonds
conda install lxml
pip install -r requirements.txt
replace any of (line 2)
    - {% load staticfiles %}
    - {% load static from staticfiles %}
    - {% load admin_static %}
with {% load static %} @ C:\Users\user\miniconda3\envs\chinabonds\Lib\site-packages\rest_framework_swagger\templates\rest_framework_swagger
will render once boot

To boot,
>> ./manage.py migrate
>> C:\Users\user\AppData\Local\JetBrains\Toolbox\apps\PyCharm-P\ch-0\203.7717.65\bin\runnerw64.exe C:\Users\user\miniconda3\envs\chinabonds\python.exe C:\Users\user\AppData\Local\JetBrains\Toolbox\apps\PyCharm-P\ch-0\203.7717.65\plugins\python\helpers\pydev\pydevd.py --multiproc --qt-support=auto --client 127.0.0.1 --port 57007 --file C:/Users/user/PycharmProjects/NextChinaConverts/manage.py runserver 8000
```

## Render once boot
![swagger.png](swagger.png)

## Demo
See api_demo.mp4, thank you.

## Further Improvements
Add the conversation ratios, interest ladder, soft and hard provisions of the CB
Most of the time, its simpler to elect to receive and sell once listed / able from what I am told